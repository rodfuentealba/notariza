import React from 'react';
import { Layout, Menu } from 'antd';
import logoNotarisa from './../img/logo/notarisa-logo.svg'

const { Header } = Layout;
const items = ['Productos', 'Soluciones', 'Contacto', 'Centro de ayuda'].map((key) => ({
  key,
  label: `${key}`,
}));


const NavBar = () => {

  return (
    <>
      <Header>
        <div className='navbar' style={{ display: "flex" }}>
          <div className="logo" style={{  }} >
            <img className='img-fluid' unoptimized alt="logo" src={logoNotarisa} />
          </div>
          <Menu
            className='ff-poppins fw-medium small'
            style={{ minWidth: 0, flex: "auto" }}
            theme="light"
            mode="horizontal"
            items={items}
          />
        </div>
      </Header>
    </>
  );
};

export default NavBar;
