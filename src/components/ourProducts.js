import React from 'react';
import { Row, Col, Typography, Button, Card } from 'antd';

import grafic1 from '../img/graphics1.svg';
import channelIcon from '../img/icons/channel-icon.svg';
import backImg1 from '../img/bg/yellow-bg.svg';
import peopleIcon from '../img/icons/people-check-icon.svg';
import backImg2 from '../img/bg/purple-bg.svg';
import grafic2 from '../img/graphics2.svg';


const { Paragraph, Title } = Typography;

const OurProductsSection = () => {

  return (
    <>
      <br />
      <br />
      <br />
      <Row justify="center">
        <Col lg={16}>
          <Paragraph className="text-center fw-medium ff-poppins text-magenta-10">NUESTROS PRODUCTOS</Paragraph>
          <Title className="text-center" style={{ marginTop: 12 }}>Digitaliza tus trámites legales y súmate a la nueva forma de cerrar acuerdos</Title>
        </Col>
      </Row>
      <br />
      <br />
      <br />
      <Row justify="center" gutter={{ lg: 104 }}>
        <Col lg={9}>
          <Card style={{
            backgroundImage: `url(${backImg1})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            marginBottom: '1rem',
            backgroundColor: '#fff1f0',
            position: 'relative',
            width: '100%',
            height: '425px'
          }}>
            <img src={grafic1} className="grafic1" alt="graficas notariza" />
          </Card>

        </Col>
        <Col lg={7}>
          <Title level={4} className="text-orange-5" style={{ fontSize: 24 }}> <img src={channelIcon} alt="channel icon" /> Docs</Title>
          <Title level={4} className="text-orange-8 fw-medium" style={{ fontSize: 24 }}>Gestiona tus documentos en un solo lugar</Title>
          <Paragraph className="text-orange-10">Estamos en cada etapa del proceso de firma de documentos. Trabaja con la mejor experiencia en gestión de trámites.</Paragraph>
          <Button className="btn-orange-7" size="large">Conoce más →</Button>
        </Col>
      </Row>
      <br />
      <br />

      <br />

      <Row justify="center" gutter={{ lg: 104 }}>
        <Col lg={7}>
          <Title level={4} className="text-magenta-5" style={{ fontSize: 24 }}> <img src={peopleIcon} alt="people check icon" /> DIP</Title>
          <Title level={4} className="text-magenta-8 fw-medium" style={{ fontSize: 24 }}>Reduce el riesgo de fraude, suplantación y alteración de documentos</Title>
          <Paragraph className="text-magenta-10">Crea procesos seguros y rápidos con nuestro protocolo de Verificación de Identidad (DIP).</Paragraph>
          <Button className="btn-magenta-7" size="large">Conoce más →</Button>
        </Col>
        <Col lg={9}>
          <Card style={{
            backgroundImage: `url(${backImg2})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            marginBottom: '1rem',
            backgroundColor: '#fff1f0',
            position: 'relative',
            width: '100%',
            height: '425px'
          }}>
            <img src={grafic2} className="grafic1" alt="graficas notariza" />
          </Card>
        </Col>
      </Row>
    </>
  );
};

export default OurProductsSection;
