import React from 'react';
import NavBar from '../components/navbar';
import Header from '../components/header';
import BrandsSection from '../components/brands';
import OurProductsSection from '../components/ourProducts';
import TransformSection from '../components/transform';
const LandingView = () => {

  return (
    <>
      <NavBar />
      <Header/>
      <BrandsSection />
      <OurProductsSection/>
      <TransformSection />
    </>
  );
};

export default LandingView;
